package com.werureo.coderswag.controller

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.werureo.coderswag.R
import com.werureo.coderswag.model.Product
import com.werureo.coderswag.utilities.EXTRA_PRODUCT
import kotlinx.android.synthetic.main.activity_product_detail.*

class ProductDetailActivity : AppCompatActivity() {

    private lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        product = intent.getParcelableExtra(EXTRA_PRODUCT)

        bind()
    }

    fun bind() {
        val resourceId = resources.getIdentifier(
                product.image,
                "drawable",
                packageName
        )

        productDetailImage.setImageResource(resourceId)
        productDetailName.text = product.title
        productDetailPrice.text = product.price
    }
}
